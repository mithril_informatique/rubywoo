
class Woocommerce
    SHOW_NO_UPDATE = false
    def initialize(config)
        @woo_connexion = WooCommerce::API.new(
            config['url'],
            config['api_ck'],
            config['api_cs'],
            {
                wp_api: true,
                version: "wc/v3"
            }
        )
        @per_page = 100
        @all_products = all_products
        @all_categories = all_categories
    end

    def get_all_products
        return @all_products
    end

    def all_products(only_visible=true)
        Log.add("Chargement de tous les articles de woocommerce")
        nb_products = 0
        @woo_connexion.get("reports/products/totals").parsed_response.each do |totals|
            nb_products = totals['total'].to_i if totals['slug']=='simple'
        end

        max_pages = (nb_products/@per_page).to_i + (nb_products.modulo(@per_page)>0 ? 1 : 0)

        products = []
        options = { per_page: @per_page }
        options.merge!({ catalog_visibility: "visible" }) if only_visible
        (1..max_pages).each do |page|
            products += @woo_connexion.get("products", options.merge(page: page)).parsed_response
        end

        return products
    end

    def search_product(sku)
        @woo_connexion.get("products", sku: sku).parsed_response.first
    end

    def local_search_product(sku)
        prod = nil
        @all_products.each do |product|
            if product['sku']==sku
                prod = product
                break
            end
        end
        return prod
    end

    def update_product(id, data)
        Log.add("> Mise à jour de l'article #{data[:name]}")
        @woo_connexion.put("products/#{id}", data).parsed_response
    end

    def delete_product(id, force=true)
        Log.add("- Suppression de l'article n° #{id}")
        @woo_connexion.delete("products/#{id}", force: force).parsed_response
    end

    def create_product(data)
        Log.add("+ Création de l'article #{data[:name]}")
        @woo_connexion.post("products", data).parsed_response
    end

    def update_or_create_product(data)
        product = local_search_product(data[:sku])
        if product
            if product['images'].count.positive?
                data.delete(:images)
            else
                data[:images].select! { |image| check_url(image[:src]) }
            end
            if egality?(product, data)
                Log.add("~ Article #{data[:name]} déjà à jour") if SHOW_NO_UPDATE
            else
                update_product(product['id'], data)
            end
        else
            data[:images].select! { |image| check_url(image[:src]) }
            create_product(data)
        end
    end

    def egality?(product, data)
        egality = data[:stock_quantity].to_i == product['stock_quantity'].to_i
        egality &= data[:name].to_s == product['name'].to_s
        egality &= data[:regular_price].to_f == product['regular_price'].to_f
        egality &= data[:categories].first[:id].to_i == product['categories'].first['id'].to_i
        egality &= data[:tax_status].to_s == product['tax_status'].to_s
        data_tags = data[:tags] ? data[:tags].map {|tag| tag[:name]} : []
        product_tags = product['tags']&.map {|tag| tag['name']}
        egality &= data_tags&.sort == product_tags&.sort
        #egality &= 2<1 # Pour forcer la mise à jour
        return egality
    end

    def update_product_from_sku(sku, data)
        product = search_product(sku)
        update_product product['id'], data
    end

    def update_all_products(data)
        products = all_products
        bar = ProgressBar.new(products.count, :counter, :bar, :percentage, :elapsed, :eta)
        products.each do |product|
            bar.increment!
            update_product(product['id'], data)
        end
    end

    def all_categories
        Log.add("Chargement de toutes les catégories de woocommerce")
        nb_categories = 0
        categories = []
        i = 1
        while 2>1
            category = @woo_connexion.get("products/categories", page: i, per_page: @per_page).parsed_response
            break unless category.count>1

            categories += category
            i += 1
        end

        return categories
    end

    def find_category_from_name(name)
        @all_categories.each do |category|
            if category['name'] == name
                return category
                break
            end
        end
        return nil
    end

    def delete_old_products(skus, fournisseur=nil)
        Log.add("Suppression d'anciens articles")
        products = @all_products
        products.each do |product|
            attr_fournisseur = product['attributes'].select{ |a| a['name']=="Fournisseur" }.first
            supplier = attr_fournisseur ? attr_fournisseur['options']&.first : nil
            if (fournisseur && fournisseur==supplier) || !fournisseur
                delete_product(product['id']) if !skus.include?(product['sku']) || (product['stock_quantity'].to_i < 1)
            end
        end
    end

    def import(products, fournisseur=nil, delete=true)
        products.each do |product|
            data = product
            category_id = find_category_from_name(data[:category_name]).first.last
            data.delete(:category_name)
            data.merge!({categories: [{ id: category_id }]})
            update_or_create_product(data)
        end
        skus = products.map {|product| product[:sku]}
        delete_old_products(skus, fournisseur) if delete
    end

    def check_url(initial_url)
        url = URI.parse(initial_url)
        req = Net::HTTP.new(url.host, url.port)
        req.use_ssl = true
        res = req.request_head(url.path)
        return res.code == "200"
    end
end