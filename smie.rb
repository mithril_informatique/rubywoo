class Smie
    def initialize(config)
        Log.add("Initialisation de la connexion au site SMIE")
        @smie = Mechanize.new
        page = @smie.get config['url']
        form = page.forms.first
        form._username = config['username']
        form._password = config['password']
        @smie.submit(form, form.buttons.first)
        @marge = config['marge']
        @categories = YAML.load_file(config['categories'])
        @actual_skus = []
        @actual_product_selection = []
    end

    def product(product_url, category_name=nil, tag=nil, conditional_tag=nil)
        # recherche smie :
        page = @smie.get product_url
        images = page.search("picture").children.map do |child|
            child.attribute_nodes&.first&.name == 'data-srcset' ? { src: child.attribute_nodes&.first&.value.gsub('_thumbnail', '') } : nil
        end
        images&.compact!&.uniq!
        images&.select! {|image| image[:src].include?('/product_details/') }
        #images.select! {|image| check_url(image[:src]) }
        tva = page.search('ol>li>a').map(&:content).last.include?('(tva)')
        short_desc = page.search('ol>li').last.content
        mini_desc = page.search('h1').first.content
        supplier_price = page.search('li.h4').children.first.content.gsub(',','.').gsub(' ','').to_f
        price = supplier_price*@marge
        price *= 1.085 if tva
        stock_st_louis = page.search('dd')[1].content.to_i
        stock_la_mare = page.search('dd')[0].content.to_i
        stock_st_pierre = page.search('dd')[2].content.to_i
        stock_total = stock_st_louis + stock_st_pierre + stock_la_mare
        #eco_part = page.search(".d-flex>.col>span").children.first&.content&.gsub('Éco-participation DEEE ','').to_f
        datas = {   name: short_desc,
                    sku: page.search('dd')[3].content,
                    ean: page.search('dd')[4].content,
                    regular_price: price.round(0).to_s,
                    stock_quantity: stock_total.to_s,
                    manage_stock: 'true',
                    stock_status: stock_total>0 ? 'instock' : 'outofstock',
                    short_description: page.search('p').first.content,
                    description: page.search('#attributes>.row>.col>table').to_s,
                    category_name: category_name,
                    attributes: [{name: "Fournisseur", visible: 'false', options: ["SMIE"]}],
                    images: images,
                    tax_status: tva ? 'taxable' : 'none',
                    supplier_price: supplier_price
                }
        if tag
            datas.merge!({ tags: [{name: tag}] })
        elsif conditional_tag
            tag_name = short_desc.include?(conditional_tag['term']) ? conditional_tag['if_true'] : conditional_tag['if_false']
            datas.merge!({ tags: [{name: tag_name }] }) if tag_name
        end
        if stock_total >= 3
            @actual_product_selection << datas
        end
        return datas
    end

    def products_from_category(category_url, category_name, filter=nil, tag=nil, conditional_tag=nil)
        page = @smie.get "/#{category_url}"
        nb_pages = 1
        while 2>1   
            break unless page.link_with(href: "/#{category_url}/?page=#{nb_pages+1}")
            nb_pages += 1
        end
        products_links = []
        (1..nb_pages).each do |n|
            page = @smie.get "/#{category_url}/?page=#{n}" if n>1
            links = page.search("a.text-dark").map do |a|
                {name: a.children.first.content, link: a.attribute_nodes&.last&.value}
            end
            links.select! { |link| link[:name].include?(filter) } if filter
            products_links += links.map { |link| link[:link] }
        end
        products_links.each { |product_link| product(product_link, category_name, tag, conditional_tag) }
    end

    def all_products
        @actual_skus = []
        @actual_product_selection = []
        @categories.each do |key, value|
            Log.add("-> Récupération des articles de la catégorie #{key}")
            value.each do |link|
                filters = link['filters'] || [nil] 
                filters.each do |filter|
                    products_from_category(link['url'], key.to_s, filter, link['tag'], link['conditional_tag'])
                end
            end
        end
        return @actual_product_selection
    end



    def get_actual_skus
        return @actual_skus
    end

    def get_actual_product_selection
        return @actual_product_selection
    end

    def check_url(initial_url)
        url = URI.parse(initial_url)
        req = Net::HTTP.new(url.host, url.port)
        req.use_ssl = true
        res = req.request_head(url.path)
        return res.code == "200"
    end

end