require 'bundler'
require 'woocommerce_api'
require 'yaml'
require 'net/http'
require 'mechanize'
require 'optparse'
require './log.rb'
require './smie.rb'
require './mc3.rb'
require './dolibarr.rb'
require './woocommerce.rb'

INITIALIZE_WOOCOMMERCE = true
INITIALIZE_SMIE = true
INITIALIZE_MC3 = true

def difference_between_times(date1, date2)
    minutes = ((date2-date1)* 24 * 60).to_i
    seconds = (((date2-date1)* 24 * 60 * 60).to_i).modulo(60)
    return "#{minutes} minutes et #{seconds} secondes"
end

config = YAML.load_file('./config.yml')

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: rubywoo.rb [options]"

  opts.on("-y", "--synchronize", "Run synchronization") do |v|
    options[:synchro] = v
  end

  opts.on("-u", "--url URL", "Require url to import product url from supplier") do |v|
    options[:url] = v
  end

  opts.on("-s", "--supplier SUPPLIER", "Require supplier to import product url from supplier") do |v|
    options[:supplier] = v
  end

  opts.on("-p", "--product PRODUCT_REF", "Get product info from dolibarr") do |v|
    options[:product] = v
  end

  opts.on("--changeDolAccounts FROM_ACCOUNT-TO_ACCOUNT", "Update Dolibarr's products accounts") do |v|
    accounts = v.split('-')
    options[:from_account] = accounts.first
    options[:to_account] = accounts.last
  end

  opts.on("--dryrun", "Dry Run") do |v|
    options[:dryrun] = true
  end
end.parse!

if options[:product]
    dolibarr = Dolibarr.new(config['dolibarr'])
    product = dolibarr.get("products/ref/#{options[:product].gsub('/','_')}")
    p product
end

if options[:supplier]
    supplier = nil
    if options[:supplier]=='SMIE'
        supplier = Smie.new(config['smie'])
    elsif options[:supplier]=='MC3'
        supplier = Mc3.new(config['mc3'])
    else
        puts 'Unknown supplier (must be SMIE or MC3)'
        return 
    end
    
    while true
        url = options[:url]
        unless url
            puts "URL:"
            url = gets.chomp
            break if url=='q'
        end
        product = supplier.product(url)
        if product
            dolibarr = Dolibarr.new(config['dolibarr'])
            product_exist = dolibarr.get("products/ref/#{product[:sku].gsub('/','_')}")
            if product_exist
                if dolibarr.update_product(product_exist[:id], dolibarr.convert(product))
                    puts "Produit mis à jour dans Dolibarr"
                else
                    puts "Erreur lors de la mise à jour"
                end
            else
                if dolibarr.create_product(dolibarr.convert(product))
                    puts "Produit créé dans Dolibarr"
                else
                    puts "Erreur lors de la création"
                end
            end
        else
            puts "Produit introuvable"
        end
        options[:url] = nil
    end
    
end

if options[:synchro]
    initial_date = DateTime.now
    start_date = initial_date
    Log.add("\n\n===============\nDébut de la procédure le #{start_date.strftime('%d/%m/%Y à %Hh%Mm%S')}\n===============")

    if INITIALIZE_WOOCOMMERCE
        woocommerce = Woocommerce.new(config['woocommerce'])
        end_date = DateTime.now
        Log.add("Fin de la récupération Woocommerce en #{difference_between_times(start_date, end_date)}\n")
        start_date = end_date
    end

    if INITIALIZE_SMIE
        smie = Smie.new(config['smie'])
        woocommerce.import(smie.all_products, 'SMIE')
        end_date = DateTime.now
        Log.add("Fin de la récupération SMIE en #{difference_between_times(start_date, end_date)}\n")
        start_date = end_date
    end

    if INITIALIZE_MC3
        mc3 = Mc3.new(config['mc3'])
        woocommerce.import(mc3.all_products, 'MC3')
        Log.add("Fin de la récupération MC3 en #{difference_between_times(start_date, DateTime.now)}")
    end

    Log.add("===============\nFin de la procédure le #{DateTime.now.strftime('%d/%m/%Y à %Hh%Mm%S')} en #{difference_between_times(initial_date, DateTime.now)}\n===============")
end

if options[:from_account]
    Log.add("Changement de compte comptable de #{options[:from_account]} à #{options[:to_account]}")
    dolibarr = Dolibarr.new(config['dolibarr'])
    products = dolibarr.get("products", limit: 10000)
    count = 0
    products.each do |p|
        if p[:accountancy_code_sell] == options[:from_account]
            count += 1
            dolibarr.update_product(p[:id], accountancy_code_sell: options[:to_account]) unless options[:dryrun]
        elsif p[:accountancy_code_buy] == options[:from_account]
            count += 1
            dolibarr.update_product(p[:id], accountancy_code_buy: options[:to_account]) unless options[:dryrun]
        end
    end
    Log.add "#{count} article(s) mis à jour #{options[:dryrun] ? '(dry run)' : ''}"
end

