class Mc3
    def initialize(config)
        Log.add("Initialisation de la connexion au site MC3")
        @connexion = Mechanize.new
        page = @connexion.get config['url']
        form = page.forms.first
        form.Username = config['username']
        form.Password = config['password']
        @connexion.submit(form, form.buttons.first)
        @marge = config['marge']
        @categories = YAML.load_file(config['categories'])
        @actual_product_selection = []
    end

    def product(article, category_name=nil, options=nil)
        
        if article.is_a?(String)
            article = @connexion.get "/#{article}"
        end

        ref = article.search('.article-reference').children.first&.content
        return nil unless ref
        name = article.search('.article-designation').children.first.content
        name.gsub!('DESTOCKAGE','')
        name.gsub!('DESTOCK','')
        name.gsub!('PROMO','')
        name.gsub!('promo','')
        name.gsub!('destockage','')
        name.gsub!('destock','')
        name.gsub!('NEW ','')
        name.gsub!('new ','')
        name.gsub!('!!!!!','')
        name.gsub!('!!!!','')
        name.gsub!('!!!','')
        name.strip!
        tva = false
        article.search('.article-taxes span').each do |taxe|
            tva = true if taxe.children.first.content.include?("TVA")
        end
        supplier_price = article.search('.article-price').children.first.content.gsub(' ','').to_f
        price = supplier_price * @marge
        price *= 1.085 if tva
        stock = 0
        article.search('.stocks tr').each do |l|
            depot = l.search('td').children.first.content.strip
            qty = l.search('th').children.first.content.to_i
            stock += qty if ["Nord", "Sud"].include?(depot)
        end
        image = article.search('img').first.attributes["src"].content
        datas = {   name: name,
                    sku: ref,
                    regular_price: price.round(0).to_s,
                    stock_quantity: stock.to_s,
                    manage_stock: 'true',
                    stock_status: stock>0 ? 'instock' : 'outofstock',
                    category_name: category_name,
                    attributes: [{name: "Fournisseur", visible: 'false', options: ["MC3"]}],
                    images: [{ src: image }],
                    tax_status: tva ? 'taxable' : 'none',
                    supplier_price: supplier_price
                }
        datas.merge!({ tags: [{name: options[:tag]}] }) if options&[:tag]
        datas[:attributes] << options[:attributes] if options&[:attributes]
        datas[:attributes] << format_attribute(datas, options[:conditional_attribute]) if options&[:conditional_attribute]
        if stock.positive?
            @actual_product_selection << datas
        end
        return datas
    end

    def format_attribute(datas, conditional_attribute)
        nil
    end

    def products_from_category(category_url, category_name, options=nil)
        page = @connexion.get "/#{category_url}"
        nb_pages = 1
        while 2>1   
            break unless page.link_with(href: "#{nb_pages+1}")
            nb_pages += 1
        end
        (1..nb_pages).each do |n|
            page = @connexion.get "/#{category_url}?page=#{n}" if n>1
            liste = page.search('.article').each do |article|
                product(article, category_name, options)
            end
        end
    end

    def all_products
        @actual_product_selection = []
        @categories.each do |key, value|
            Log.add("-> Récupération des articles de la catégorie #{key}")
            value.each do |link|
                filters = link['filters'] || [nil] 
                filters.each do |filter|
                    options = { filter: filter,
                                tag: link['tag'],
                                attributes: link['attributes'],
                                conditional_attribute: link['conditional_attribute']
                              }
                    products_from_category(link['url'], key.to_s, options)
                end
            end
        end
        return @actual_product_selection
    end

    def get_connexion
        return @connexion
    end

    def get_actual_product_selection
        return @actual_product_selection
    end

    def reset_actual_product_selection
        @actual_product_selection = []
    end

end