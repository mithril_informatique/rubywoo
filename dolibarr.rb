
require 'rest-client'

class Dolibarr

    def initialize(config)
        @conf = config
    end

    def get(requete, params={})
        url = "#{@conf['url']}/#{requete}"
        begin
            result = RestClient.get url,
                                    params: params.merge(DOLAPIKEY: @conf['dolapikey'])
            return eval(result.body.gsub('null', 'nil'))
        rescue
            return nil
        end
    end

    def create_product(data)
        result = false
        begin
            result = RestClient.post "#{@conf['url']}/products",
                                     data,
                                     { DOLAPIKEY: @conf['dolapikey'] }
        rescue RestClient::ExceptionWithResponse => e
            error = JSON.parse(e.response)['error'].to_a.join(' ')
            puts error
            result = false
        end
        return result
    end

    def update_product(id, data)
        result = false
        begin
            result = RestClient.put "#{@conf['url']}/products/#{id}",
                                     data,
                                     { DOLAPIKEY: @conf['dolapikey'] }
        rescue RestClient::ExceptionWithResponse => e
            error = JSON.parse(e.response)['error'].to_a.join(' ')
            puts error
            result = false
        end
        return result
    end

    def convert(data)
        data.transform_keys!(sku: :ref, name: :label, short_description: :description, regular_price: :price_ttc, ean: :barcode, supplier_price: :cost_price)
        price_HT = data[:price_ttc].to_f
        tva = data[:tax_status]=='taxable'
        price_HT /= 1.085 if tva
        data[:price] = price_HT
        data[:status] = "1"
        data[:status_buy] = "1"
        data[:barcode_type] = "2"
        data[:default_vat_code] = tva ? "85" : "0"
        data[:tva_tx] = tva ? "8.500" : "0.000"
        data[:accountancy_code_sell] = tva ? "707" : "7071"
        data[:accountancy_code_buy] = "607"
        data[:description] = " "
        data.delete(:stock_quantity)
        data.delete(:manage_stock)
        data.delete(:stock_status)
        data.delete(:category_name)
        data.delete(:attributes)
        data.delete(:tax_status)
        data.delete(:images)

        if data[:barcode] && data[:barcode].length>0 
            while data[:barcode].length<13 do
                data[:barcode].prepend('0')
            end
        end

        return data
    end

end
